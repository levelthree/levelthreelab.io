use ARMS_dev;
if not exists (select * from sys.schemas where sys.schemas.name like 'KushalUniversity')
	begin
		exec('create schema [KushalUniversity] authorization [dbo]');
		exec('select 0');
	end 
else 
	begin
		select 1;
	end

select * from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME like 'kushal%';

--drop schema [KushalUniversity];
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA like 'KushalUniversity' and TABLE_NAME like 'KUSHALEDGE')
	begin 
		select 1
		DROP TABLE [KushalUniversity].[KUSHALEDGE];	
	end
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA like 'KushalUniversity' and TABLE_NAME like 'KUSHALNODE')
	begin 
		select 1
		DROP TABLE [KushalUniversity].[KUSHALNODE];
	end  
begin 
	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	CREATE TABLE [KushalUniversity].[KUSHALNODE]
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[NAME] NVARCHAR(MAX) NOT NULL,
		[DESCRIPTION] NVARCHAR(MAX) NOT NULL,
		[LATITUDE] DECIMAL(9,6) NOT NULL,
		[LONGITUDE] DECIMAL(9,6) NOT NULL,
		[ROWVERSION] [timestamp] NOT NULL,
		CONSTRAINT [PK_KUSHALNODE] PRIMARY KEY ([ID])
	);
	select 0;
	CREATE TABLE [KushalUniversity].[KUSHALEDGE]
	(
		[ID] [INT] IDENTITY(1,1) NOT NULL,
		[NAME] NVARCHAR(MAX) NOT NULL,
		[DESCRIPTION] NVARCHAR(MAX) NOT NULL,
		[AEND] INT NOT NULL,
		[ZEND] INT NOT NULL,
		[ROWVERSION] [timestamp] NOT NULL,
		CONSTRAINT [PK_KUSHALEDGE] PRIMARY KEY ([ID]),
		CONSTRAINT [FK_KUSHALEDGE_KUSHALNODEA] FOREIGN KEY ([AEND]) REFERENCES [KushalUniversity].[KUSHALNODE] ([ID]) ON DELETE NO ACTION, 
		CONSTRAINT [FK_KUSHALEDGE_KUSHALNODEZ] FOREIGN KEY ([ZEND]) REFERENCES [KushalUniversity].[KUSHALNODE] ([ID]) ON DELETE NO ACTION
	)
	INSERT INTO [KushalUniversity].[KUSHALNODE] ([NAME], [DESCRIPTION], [LATITUDE], [LONGITUDE]) VALUES ('HOME', 'this is just a test', 39.8717646, -104.9383711);
	INSERT INTO [KushalUniversity].[KUSHALNODE] ([NAME], [DESCRIPTION], [LATITUDE], [LONGITUDE]) VALUES ('WORK', 'this is just a test', 39.9218875, -105.1456117);
	INSERT INTO [KUSHALUNIVERSITY].[KUSHALEDGE] ([NAME], [DESCRIPTION], [AEND], [ZEND]) VALUES ('FIBER ONE', 'DIRECT CONNECTION FROM WORK TO HOME', 1, 2);
	SELECT * FROM [KUSHALUNIVERSITY].[KUSHALNODE];
	SELECT * FROM [KUSHALUNIVERSITY].[KUSHALEDGE];
end

--drop schema [KushalUniversity];