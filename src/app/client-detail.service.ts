import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ClientDetail } from './client-detail';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ClientDetailService {
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  clientDetailUrl = this.baseUrl + 'assets/clientdata.json';

  getUserDetail() {
    return this.http.get<ClientDetail[]>(this.clientDetailUrl)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  addClientDetail(clientDetail: ClientDetail): Observable<ClientDetail> {
    return this.http.post<ClientDetail>(this.clientDetailUrl, clientDetail, httpOptions)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          const data = {
              reason: error && error.error.reason ? error.error.reason : '',
              status: error.status
          };
          return throwError(error);
      })
      );
  }
}
