import { Component, OnInit, Inject, Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClientDetail } from '../client-detail';
import { ClientDetailService } from '../client-detail.service';


@Component({
  selector: 'app-fetch-client-detail',
  templateUrl: './fetch-client-detail.component.html',
  styleUrls: ['./fetch-client-detail.component.css']
})
export class FetchClientDetailComponent implements OnInit {

  public clientDetails: ClientDetail[];
  public clientDetailsView: ClientDetail[];
  public clientCitiesDistinct: Array<string>;
  public clientLegacyNamesDistinct: Array<string>;

  public viewModelUniqueIdentifier: string;
  public viewModelName: string;
  public viewModelLegacyName: string;
  public viewModelLegacyNames: Array<string>;
  public viewModelYearEstablished: string;
  public viewModelCity: string;
  public viewModelCities: Array<string>;
  public viewModelState: string;

  public p_viewModelUniqueIdentifier: string;
  public p_viewModelName: string;
  public p_viewModelLegacyName: string;
  public p_viewModelLegacyNames: Array<string>;
  public p_viewModelYearEstablished: string;
  public p_viewModelCity: string;
  public p_viewModelCities: Array<string>;
  public p_viewModelState: string;

  public load: boolean;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private clientDetailService: ClientDetailService) { }

  ngOnInit() {
    this.load = false;

    this.clientDetailService.getUserDetail().subscribe(result => {
      this.clientDetails = result;
      this.clientDetailsView = result;
      for (const element of result) {
        this.clientCitiesDistinct.push(element.city);
        this.clientLegacyNamesDistinct.push(element.legacyName);
      }
      this.clientCitiesDistinct = this.clientCitiesDistinct.filter((value, index, array) => array.indexOf(value) === index).sort();
      this.clientLegacyNamesDistinct
        = this.clientLegacyNamesDistinct.filter((value, index, array) => array.indexOf(value) === index).sort();

      this.p_viewModelUniqueIdentifier = this.viewModelUniqueIdentifier;
      this.p_viewModelName = this.viewModelName;
      this.p_viewModelLegacyName = this.viewModelLegacyName;
      this.p_viewModelYearEstablished = this.viewModelYearEstablished;
      this.p_viewModelCity = this.viewModelCity;
      this.p_viewModelCities = this.viewModelCities;
      this.p_viewModelState = this.viewModelState;
      this.p_viewModelLegacyNames = this.viewModelLegacyNames;

      this.load = true;
    }, error => console.error(error));
  }

  resetFilters() {
    this.clientDetailsView = this.clientDetails;
    this.viewModelUniqueIdentifier = '';
    this.viewModelName = '';
    this.viewModelLegacyName = '';
    this.viewModelYearEstablished = '';
    this.viewModelCity = '';
    this.viewModelCity = '';
    this.viewModelState = '';
    this.viewModelLegacyNames = new Array<string>();
    this.viewModelCities = new Array<string>();
  }

  filterEverything(e: { path: { id: string; }[]; target: { value: string; }; }) {
    console.log(this.viewModelLegacyNames);
    this.clientDetailsView = this.clientDetails;
    if (this.viewModelUniqueIdentifier && this.viewModelUniqueIdentifier.trim() !== '') {
      this.clientDetailsView = this.clientDetailsView.filter(x => x.uniqueIdentifier === this.viewModelUniqueIdentifier);
    }
    if (this.viewModelName && this.viewModelName.trim() !== '') {
      this.clientDetailsView = this.clientDetailsView.filter(x => x.name === this.viewModelName);
    }
    // if (this.viewModelLegacyName && this.viewModelLegacyName.trim() !== '') {
    //   this.clientDetailsView = this.clientDetailsView.filter(x => x.legacyName === this.viewModelLegacyName);
    // }
    if (this.viewModelLegacyNames) {
      this.viewModelLegacyNames.forEach(x => {
        console.log(x);
      });
      this.clientDetailsView = this.clientDetailsView.filter(f => this.viewModelLegacyNames.includes(f.legacyName));
      if (this.viewModelLegacyNames.length === 1 && this.viewModelLegacyNames[0] === '') {
        this.resetFilters();
      }
    }
    if (this.viewModelYearEstablished && this.viewModelYearEstablished.trim() !== '') {
      this.clientDetailsView = this.clientDetailsView.filter(x => x.yearEstablished === this.viewModelYearEstablished);
    }
    // if (this.viewModelCity && this.viewModelCity.trim() !== '') {
    //   this.clientDetailsView = this.clientDetailsView.filter(x => x.city === this.viewModelCity);
    // }
    if (this.viewModelCities) {
      this.clientDetailsView = this.clientDetailsView.filter(f => this.viewModelCities.includes(f.city));
    }
    if (this.viewModelCities.length === 1 && this.viewModelCities[0] === '') {
      this.resetFilters();
    }
    if (this.viewModelState && this.viewModelState.trim() !== '') {
      this.clientDetailsView = this.clientDetailsView.filter(x => x.state === this.viewModelState);
    }
  }

  createAndAddRandomClient() {
    const newClientDetail: ClientDetail = {
      id: 1,
      uniqueIdentifier: this.newGuid(),
      name: this.makeRandomString(5),
      legacyName: this.makeRandomString(5),
      yearEstablished: '1947',
      streetAddress: this.makeRandomString(5),
      city: this.makeRandomString(5),
      state: this.makeRandomString(2),
      accountManager: this.makeRandomString(5),
      email: this.makeRandomString(5) + '@' + this.makeRandomString(5) + '.' + this.makeRandomString(3)
    };
    this.addClientDetail(newClientDetail);
  }

  addClientDetail(newClientDetail: ClientDetail) {
    this.clientDetailService.addClientDetail(newClientDetail)
      .subscribe(clientDetail => this.clientDetails.push(clientDetail));
  }

  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0,
        v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  makeRandomString(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
