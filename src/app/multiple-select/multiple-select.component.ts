import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiple-select',
  templateUrl: './multiple-select.component.html',
  styleUrls: ['./multiple-select.component.css']
})
export class MultipleSelectComponent implements OnInit {

  @Input() inputStringArray: Array<string>;
  @Output() emittedStringArray: EventEmitter<Array<string>> = new EventEmitter<Array<string>>();

  constructor() { }

  ngOnInit() {
  }

}
