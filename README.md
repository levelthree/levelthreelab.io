
```csharp
using System;
using System.Collections.Generic;
using System.Linq;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

class Solution {
    public int solution(int[] A) {
        // write your code in C# 6.0 with .NET 4.5 (Mono)
        var removedDuplicates = A.Distinct().ToArray();
        Array.Sort(removedDuplicates);
        int counter = 1;
        int current = 0;
        foreach (int element in removedDuplicates)
        {
            if (element > 0)
            {
                if (counter < element) 
                {
                    return counter;
                }
                counter++;
            }
        }
        return counter;
    }
}
```